# css 样式

## Border 调试法

- 怀疑某个元素有问题
- 就给这个元素加 border
- border 没出现，说明选择器错了或者语法错了
- border 出现了，看看边界是否符合预期
- bug 解决了才把 border 删掉

  border:1px solid red;

## 永远不要写宽度 100%

## overflow 溢出

- 当内容的宽度或者高度大于容器，会溢出
- 可用 overflow 来设置是否显示滚动条
- auto 是灵活设置
- scroll 是永远显示
- hidden 是直接隐藏溢出部分
- visible 是直接显示溢出部分
- overflow 可以分为 overflow-x 和 overflow-y

## margin 外边距

## padding 内边距

## float position: absolute fixed 脱离文档流
 

# 面试问题

## 解释盒模型？

一种 content-box，一种 border-box。content-box 宽度只包括 content。border-box 包括 border，padding，content。

## margin 合并
